﻿using System.Reflection;
using System.Web.Mvc;
using ApriCodeTest.Infrastructure.Autofac;
using Autofac;
using Autofac.Integration.Mvc;

namespace ApriCodeTest.Web
{
    public class RegisterConfig
    {
        public static void Register()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly())
                .InstancePerRequest();
            builder.RegisterFilterProvider();
            builder.RegisterModule(new AutofacWebTypesModule());
            builder.RegisterModule(new ModuleBuilder(builder));
            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}