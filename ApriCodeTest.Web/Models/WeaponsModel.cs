﻿using System.Collections.Generic;
using ApriCodeTest.Infrastructure.Model.Domain;

namespace ApriCodeTest.Web.Models
{
    public class WeaponsModel
    {
        public WeaponsModel(List<Weapon> weapons)
        {
            Weapons = weapons;
        }

        public List<Weapon> Weapons { get; }
    }
}