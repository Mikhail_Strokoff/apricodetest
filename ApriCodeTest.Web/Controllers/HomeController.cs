﻿using System.Web.Mvc;
using ApriCodeTest.Infrastructure.Model.Container;
using ApriCodeTest.Infrastructure.Service.Interface;
using ApriCodeTest.Web.Models;

namespace ApriCodeTest.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IWeaponService _weaponService;

        public HomeController(IWeaponService weaponService)
        {
            _weaponService = weaponService;
        }

        // GET: Home
        public ActionResult Index()
        {
            return View(new WeaponsModel(_weaponService.Get()));
        }

        //POST: Create
        [HttpPost]
        public ActionResult Create(string type, string name)
        {
            _weaponService.Insert(new WeaponContainer()
            {
                Type = type,
                Name = name
            });
            return Index();
        }

        //POST: Update
        [HttpPost]
        public ActionResult Update(int id, string type, string name)
        {
            _weaponService.Update(new WeaponContainer()
            {
                Id = id,
                Type = type,
                Name = name
            });
            return Index();
        }
    }
}