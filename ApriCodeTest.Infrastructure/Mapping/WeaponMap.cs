﻿using System.Data.Entity.ModelConfiguration;
using ApriCodeTest.Infrastructure.Model.Domain;

namespace ApriCodeTest.Infrastructure.Mapping
{
    public class WeaponMap : EntityTypeConfiguration<Weapon>
    {
        public WeaponMap()
        {
            ToTable("weapon");
            HasKey(w => w.Id);
            Property(w => w.Type).IsOptional().HasMaxLength(50);
            Property(w => w.Name).IsOptional().HasMaxLength(100);
        }
    }
}