﻿using System.Data.Entity;
using ApriCodeTest.Infrastructure.Model.DbContext;
using ApriCodeTest.Infrastructure.Model.Domain;
using ApriCodeTest.Infrastructure.Service;
using ApriCodeTest.Infrastructure.Service.Interface;
using Autofac;

namespace ApriCodeTest.Infrastructure.Autofac
{
    public class ModuleBuilder : Module
    {
        private readonly ContainerBuilder _builder;

        public ModuleBuilder(ContainerBuilder builder)
        {
            _builder = builder;
            Register();
        }

        public void Register()
        {
            _builder.RegisterType<WeaponService>().As<IWeaponService>();
            _builder.RegisterType<WeaponDbContext>().As<IDbContext<Weapon>>();
        }
    }
}