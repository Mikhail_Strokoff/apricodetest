﻿using ApriCodeTest.Infrastructure.Model.Domain;
using System.Collections.Generic;
using ApriCodeTest.Infrastructure.Model.Container;

namespace ApriCodeTest.Infrastructure.Service.Interface
{
    public interface IWeaponService
    {
        List<Weapon> Get();
        Weapon Get(int id);
        bool Insert(WeaponContainer weapon);
        bool Delete(int id);
        bool Update(WeaponContainer weapon);
    }
}
