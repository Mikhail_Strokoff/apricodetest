﻿using System.Collections.Generic;
using System.Linq;
using ApriCodeTest.Infrastructure.Model.Container;
using ApriCodeTest.Infrastructure.Model.Domain;
using ApriCodeTest.Infrastructure.Service.Interface;

namespace ApriCodeTest.Infrastructure.Service
{
    public class WeaponMoqService : IWeaponService
    {
        private readonly List<Weapon> _weapons;

        public WeaponMoqService()
        {
            #region Data

            _weapons = new List<Weapon>
            {
                new Weapon
                {
                    Id = 1,
                    Type = "Меч",
                    Name = "Короткий меч"
                },
                new Weapon
                {
                    Id = 2,
                    Type = "Меч",
                    Name = "Полуторный меч"
                },
                new Weapon
                {
                    Id = 3,
                    Type = "Меч",
                    Name = "Двуручный меч"
                },
                new Weapon
                {
                    Id = 4,
                    Type = "Топор",
                    Name = "Колун"
                },
                new Weapon
                {
                    Id = 5,
                    Type = "Топор",
                    Name = "Секира"
                },
                new Weapon
                {
                    Id = 6,
                    Type = "Меч",
                    Name = "Полуторный меч"
                },
                new Weapon
                {
                    Id = 5,
                    Type = "Арбалет",
                    Name = "осадный арбалет"
                },
                new Weapon
                {
                    Id = 6,
                    Type = "Арбалет",
                    Name = "Легкий арбалет"
                }
            };

            #endregion
        }

        public bool Delete(int id)
        {
            return _weapons.Remove(_weapons.First(item => item.Id == id));
        }

        public List<Weapon> Get()
        {
            return _weapons;
        }

        public Weapon Get(int id)
        {
            return _weapons.First(item => item.Id == id);
        }

        public bool Insert(WeaponContainer weapon)
        {
            _weapons.Add(new Weapon
            {
                Type = weapon.Type,
                Name = weapon.Name
            });
            return true;
        }

        public bool Update(WeaponContainer weapon)
        {
            Get(weapon.Id).Type = weapon.Type;
            Get(weapon.Id).Name = weapon.Name;
            return true;
        }
    }
}