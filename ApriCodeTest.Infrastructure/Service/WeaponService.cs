﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ApriCodeTest.Infrastructure.Model.Container;
using ApriCodeTest.Infrastructure.Model.DbContext;
using ApriCodeTest.Infrastructure.Model.Domain;
using ApriCodeTest.Infrastructure.Service.Interface;

namespace ApriCodeTest.Infrastructure.Service
{
    public class WeaponService : IWeaponService
    {
        private readonly DbContext<Weapon> _dbContext;

        public WeaponService()
        {
            _dbContext = new WeaponDbContext();
        }

        public bool Delete(int id)
        {
            _dbContext.Set<Weapon>().Remove(Get(id));
            return true;
        }

        public List<Weapon> Get()
        {
            return _dbContext.Set<Weapon>().ToList();
        }

        public Weapon Get(int id)
        {
            return _dbContext.Set<Weapon>().First(weapon => weapon.Id == id);
        }

        public bool Insert(WeaponContainer weapon)
        {
            _dbContext.Set<Weapon>().Add(new Weapon
            {
                Type = weapon.Type,
                Name = weapon.Name
            });
            return true;
        }

        public bool Update(WeaponContainer weapon)
        {
            Get(weapon.Id).Type = weapon.Type;
            Get(weapon.Id).Name = weapon.Name;
            return true;
        }
    }
}