﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ApriCodeTest.Infrastructure.Model.Domain
{
    public class Weapon
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
    }
}