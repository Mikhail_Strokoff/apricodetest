﻿using ApriCodeTest.Infrastructure.Model.Domain;

namespace ApriCodeTest.Infrastructure.Model.Container
{
    public class WeaponContainer
    {
        public WeaponContainer()
        {
        }

        public WeaponContainer(Weapon weapon)
        {
            Id = weapon.Id;
            Type = weapon.Type;
            Name = weapon.Name;
        }

        public int Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
    }
}