﻿using System.Data.Entity;
using ApriCodeTest.Infrastructure.Model.Domain;

namespace ApriCodeTest.Infrastructure.Model.DbContext
{
    public class WeaponDbContext : DbContext<Weapon>
    {
        public DbSet<Weapon> Weapons { get; set; }

        public WeaponDbContext()
        {
            var a = Database.Connection.ConnectionString;
        }
    }
}